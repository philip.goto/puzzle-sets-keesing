# Crosswords Puzzle Set for Keesing

This repo contains Dutch puzzle set downloaders for the app [Crosswords](https://gitlab.gnome.org/jrb/crosswords). The puzzles are pulled from web.keesing.com and converted to ipuz format supported by Crosswords.
