#!/usr/bin/env sh

runs=0
fails=0
succsses=0

for config in puzzle-sets/*/puzzle.config
do
	command=`grep Command=ikeesing $config | sed 's|Command=ikeesing|./ikeesing.py|g'`
	echo $command
	if $command > /dev/null
	then
		((succsses=succsses+1))
	else
		((fails=fails+1))
		echo "FAILED"
		echo
	fi
	((runs=runs+1))
done

echo "$runs tests run - $succsses succeeded - $fails failed"
